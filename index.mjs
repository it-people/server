/**
 * Node.js HTTP/2 Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru
 */

import Server from './lib/Server';
import Proxy from './lib/Proxy';

export default { Server, Proxy };
export { Server, Proxy };