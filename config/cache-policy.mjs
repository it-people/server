/**
 * Node.js Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru/
 */

export default [
  //{ match: /(html|htm)$/i, value: 'max-age="43200"' },
  //{ match: /(js|css|txt)$/i, value: 'max-age="604800"' },
  //{ match: /(flv|swf|ico|gif|jpg|jpeg|png|svg|woff)$/i, value: 'max-age="2592000"' },
  { match: /.*$/i, value: 'max-age="315360000"' },
];