/**
 * Node.JS Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru/
 */

import BaseServer from "./BaseServer";

export default class HTTP1Server extends BaseServer {
  constructor() {
    super();
  }

  run() {

  }

  compressionMiddleware() {
    return (request, response, next) => {

    };
  }
}