/**
 * Node.JS Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru/
 */

import BaseServer from "./BaseServer";

export default class HTTP2Server extends BaseServer {
  constructor () {
    super();
  }
}