import fs from 'fs';


export default function(request, response) {
  const stream = response;

  request.headers[":path"] = request.url;
  request.headers[":method"] = request.method;
  request.headers[":authority"] = request.headers.host;
  request.headers[":scheme"] = 'http';

  if ( !stream.__proto__.respond ) {
    stream.__proto__.respond = respond;
  }
  if ( !stream.__proto__.respondWithFD ) {
    stream.__proto__.respondWithFD = respondWithFD;
  }
  if ( !stream.__proto__.respondWithFile ) {
    stream.__proto__.respondWithFile = respondWithFile;
  }

  stream.closed = false;

  request.on( 'close', event => {
    stream.closed = true;
  });
  request.on( 'error', event => {
    stream.closed = true;
  });

  response.__proto__.stream = stream;

  return {
    request,
    response
  }
}
function respond(headers, options = {}) {
  setHeaders.call(this, headers);
}

function respondWithFD(url, headers, options = {}) {
  throw new Error('Не работает в http1');
  // fs.open(url,'r',(err, fd) => {})
}

function respondWithFile(url, headers, options = {}) {
  let respOptions = {};

  if (!url) {
    throw new Error('Не передан путь');
  }
  new Promise( ( resolve, reject ) => {
    fs.stat(url, (err, stat) => {
      if (err) reject(err);
      resolve(stat)
    })

  })
    .then( stat => {
      if ( !stat.isFile() ) return Promise.reject('is directory');

      if(headers[':status'] == 206){
        respOptions.start = typeof options.offset !== 'undefined' ? parseInt(options.offset, 10) : 0;
        if(isNaN(options.start)) options.start = 0;
        if(typeof options.length !== 'undefined'){
          options.length = parseInt(options.length, 10);
          if(!isNaN(options.length)){
            respOptions.end = options.length + respOptions.start;
          }
        }
      }

      setHeaders.call(this, headers);
      const readStream = fs.createReadStream(url, respOptions);
      readStream.pipe(this);
      readStream.on('error', (err) => {
        reject(err);
      });

    })
    .catch( err => {
      options.onError && options.onError(err, this);
    });
}

function setHeaders(headers = {}) {

  if ( headers[':status'] ) {
    const status = headers[':status'];
    delete headers[':status'];
    this.writeHead(status, headers);
  } else {
    for (var header in headers) {
      this.setHeader(header, headers[header]);
    }
  }

}
