/**
 * Node.js Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru
 */

import fs from 'fs';
import path from 'path';
import http from 'http';
//import http2 from 'http2';
//import http1ToHttp2 from './http1ToHttp2';
import mime from 'mime-types';
import colors from 'colors';
import {URL} from 'url';
import pjson from '../package.json';
import cachePolicyDefault from '../config/cache-policy';

const serverName = `Node.js Server v${pjson.version}`;
const dirname = path.dirname(decodeURI(new URL(import.meta.url).pathname)).replace(/^\/([A-Z]):\//, '$1:/');

const matchFileRespond = (file) => {
  return !/\.(php)$/i.test(file);
};

const mode = {
  development: 'development',
  production: 'production',
}

const rangesRegex = /^bytes=([\d]+)-([\d]+)?$/i;

const respondWrap = function (headers, options) {
  headers.server = serverName;
  //headers[http2.constants.HTTP2_HEADER_SERVER] = serverName;
  this.__respond(headers, options);
};

export default class Server {
  constructor(options = {}) {
    /**
     * Массив серверов HTTP/1 или HTTP/2
     * @type {Array}
     */
    this.servers = [];

    this.mwStream = (
      typeof options.middlewareStream !== 'undefined'
      && options.middlewareStream instanceof Array
    )
      ? options.middlewareStream
      : [];

    this.mwRequest = (
      typeof options.middlewareRequest !== 'undefined'
      && options.middlewareRequest instanceof Array
    )
      ? options.middlewareRequest
      : [];

    /**
     * Функция для проверки имени файла на возможность отправки в виде статики
     * @type {function(*=)}
     */
    this.matchFileRespond = (typeof options.matchFileRespond === 'function')
      ? options.matchFileRespond
      : matchFileRespond;
    //this.matchExtensions = /\.(jpg|jpeg|gif|png|css|js|cur|mp3|flv|avi|zip|gz|tar|rar|ico)$/i

    /**
     * Массив конфигураций серверов
     * @type {Array}
     * @private
     */
    this.__serverConfig = [];

    /**
     * Пути к виртуальным хостам
     * @type {Array}
     * @private
     */
    this.__virtualHostPaths = [];

    /**
     * Виртуальные хосты
     * @type {Object}
     * @private
     */
    this.__virtualHosts = {};
    //this.__aliasToHost = [];
  }

  /**
   * Запуск сервера
   */
  run() {
    this.__loadVirtualHosts().then(() => {
      for (let k in this.__serverConfig) {
        let config = this.__serverConfig[k];

        this.servers.push(
          /*(typeof config.secure === 'boolean' && config.secure === true)
            ? this.__runServerHttp2(config)
            : */this.__runServerHttp1(config)
        );
      }
    });
  }

  /**
   * Добавление нового сервера
   *
   * Формат конфигурации:
   * const config = {
   *   port: 443,
   *   secure: true,
   * }
   * @param config {Object} Объект конфигурации
   */
  addServer(config) {
    this.__serverConfig.push(config);
  }

  /**
   * Добавление виртуального хоста
   * @param vhost {object|string} Объект виртуального хоста или абсолютный путь
   */
  addVirtualHost(vhost) {
    if (typeof vhost === 'object') {
      this.__addVirtualHost(vhost);
    } else {
      this.__virtualHostPaths.push(vhost);
    }
  }

  /**
   * Добавление Middleware для on stream
   * @param fn
   */
  use(fn) {
    this.useStream(fn);
  }

  /**
   * Добавление Middleware для on stream
   * @param fn
   */
  useStream(fn) {
    this.mwStream.push(fn);
  }

  /**
   * Добавление Middleware для on request
   * @param fn
   */
  useRequest(fn) {
    this.mwRequest.push(fn);
  }

  /**
   * Метод импортит виртуальные хосты, добавленные в виде абсолютного пути
   * @returns {Promise.<void>}
   * @private
   */
  async __loadVirtualHosts() {
    return;

    if (this.__virtualHostPaths.length) {
      for (let k in this.__virtualHostPaths) {
        //  Конвертируем абсолютный путь в относительный
        let vhostPath = './' + path.relative(dirname, this.__virtualHostPaths[k]);
        let vhost = await import(vhostPath);
        this.__addVirtualHost(vhost.default);
      }
    }
  }

  /**
   * Добавление виртуального хоста
   * @param vhost {object} Объект виртуального хоста
   * @private
   */
  __addVirtualHost(vhost) {
    if (typeof this.__virtualHosts[vhost.name] !== 'undefined') {
      throw new Error(`Виртуальный хост ${vhost.name} уже зарегистрирован`);
      return;
    }

    //  Приводим алиасы к формату массива
    //  Блок не используется, т.к. алиасы были заменены на одну регулярку match
    // if(typeof vhost.alias === 'string'){
    //   vhost.alias = [vhost.alias];
    // }

    this.__virtualHosts[vhost.name] = vhost;
  }

  /**
   * Метод ищет виртуальный хост по имени хоста и порту.
   * В случае если хост не будет найден, вернёт хост
   * "default" (при его наличии)
   * @param hostname
   * @param port
   * @returns {*}
   * @private
   */
  __getVirtualHost(hostname, port) {
    for (let k in this.__virtualHosts) {
      if (
        typeof this.__virtualHosts[k].match !== 'undefined'
        && this.__virtualHosts[k].match.test(hostname)
        && (
          (this.__virtualHosts[k].port instanceof Array && this.__virtualHosts[k].port.indexOf(port) !== -1)
          || this.__virtualHosts[k].port === port
          || typeof this.__virtualHosts[k].port === 'undefined'
        )
      ) {
        return this.__virtualHosts[k];
      }
    }

    console.log(`Хост ${hostname} не найден`.yellow);

    if (typeof this.__virtualHosts.default !== 'undefined') {
      return this.__virtualHosts.default;
    }

    return false;
  }

  /**
   * Метод обрабатывает ответ формата HTTP/2 и вызывает хук "respond"
   * виртуального хоста.
   * При наличии в виртуальном хосте параметра staticPath,
   * будет выполнена проверка на существование статического файла
   * и отдача.
   *
   * @param stream
   * @param headers
   * @param server Экземпляр сервера http2 или http1
   * @param serverConfig Конфиг сервера, содержит порт и тип сервера (secure)
   * @private
   */
  __respond(request, response, serverConfig) {
    // if(typeof response.stream.__proto__.__respond === 'undefined'){
    //   response.stream.__proto__.__respond = response.stream.respond;
    //   response.stream.respond = respondWrap;
    // }

    let hostname = request.headers.host;
    //let hostname = request.headers[http2.constants.HTTP2_HEADER_AUTHORITY];
    if (hostname.indexOf(':') !== -1) {
      hostname = hostname.split(':')[0];
    }

    const vhost = this.__getVirtualHost(hostname, serverConfig.port);
    if (!vhost) {
      return;
    }

    //  Пробуем отдать статический файл
    const reqPath = request.url;
    const reqMethod = request.method;
    // const reqPath = request.headers[http2.constants.HTTP2_HEADER_PATH];
    // const reqMethod = request.headers[http2.constants.HTTP2_HEADER_METHOD];
    const filePath = (typeof vhost.staticPath !== 'undefined')
      ? path.join(vhost.staticPath, reqPath)
      : '';


    if (typeof vhost.staticPath === 'undefined' || !this.__respondFile(request, response, vhost)) {
      vhost.respond.call(this, request, response, this, serverConfig);
    }
  }

  /**
   * Обработка хука виртуального хоста "started"
   * @param server
   * @param config
   * @private
   */
  __started(server, config) {
    for (let k in this.__virtualHosts) {
      let vhost = this.__virtualHosts[k];

      if (typeof vhost.started === 'function') {
        vhost.started.call(this, this, config);
      }
    }
  }

  /**
   * Отдача файла сервером
   * @param stream
   * @param headers
   * @param filePath
   * @returns {boolean}
   * @private
   */
  __respondFile(request, response, vhost) {
    let reqPath = decodeURI(request.url);
    //let reqPath = request.headers[http2.constants.HTTP2_HEADER_PATH];
    if (reqPath.indexOf('?') !== -1) {
      reqPath = reqPath.split('?')[0];
    }
    const filePath = path.join(vhost.staticPath, reqPath);
    const cachePolicy = (typeof vhost.cachePolicy !== 'undefined') ? vhost.cachePolicy : cachePolicyDefault;
    let status = 200;


    //  Файла не существует
    if (!fs.existsSync(filePath) || !fs.lstatSync(filePath).isFile()) {
      return false;
    }

    let fileExt = path.extname(filePath);
    //  Файл без разширения
    // if (fileExt.indexOf('.') === -1) {
    //   return false;
    // }

    //  Файл не разрешено отдавать в виде статики
    if (!this.matchFileRespond(fileExt)) {
      return false;
    }

    const stat = fs.lstatSync(filePath);

    if (fs.existsSync(filePath) && stat.isFile()) {
      const responseMimeType = mime.lookup(filePath);

      let resHeaders = {
        'accept-range': 'bytes',
        'content-length': stat.size,
        'last-modified': stat.mtime.toUTCString(),
        'content-type': responseMimeType,
        'server': serverName,
        //[http2.constants.HTTP2_HEADER_SERVER]: serverName
      };

      if (typeof vhost.mode !== 'undefined' && vhost.mode === mode.production) {
        const fileExtname = path.extname(filePath);
        for (const cacheRule of cachePolicy) {
          if (cacheRule.match.test(fileExtname)) {
            resHeaders['cache-control'] = cacheRule.value;
            break;
          }
        }
      }

      let resOptions = {};

      //  Отдача файла по частям
      if (typeof request.headers.range !== 'undefined' && rangesRegex.test(request.headers.range)) {
        const found = request.headers.range.match(rangesRegex);
        let offset = 0;
        let end = parseInt(stat.size, 10) - 1;

        if (typeof found[1] !== 'undefined') {
          offset = parseInt(found[1], 10);
          resOptions.offset = offset;
        }

        if (typeof found[2] !== 'undefined') {
          end = parseInt(found[2], 10);
          resOptions.length = end - offset;
        }
        if (typeof resOptions.offset !== 'undefined' || typeof resOptions.length !== 'undefined') {
          resHeaders['content-range'] = `bytes ${offset}-${end}/${stat.size}`;
          resHeaders['content-length'] = end - offset + 1;
          status = 206
          //resHeaders[http2.constants.HTTP2_HEADER_STATUS] = 206;
        }
      }

      // HTTP2 - временно выпилено!!!!
      //response.stream.respondWithFile(filePath, resHeaders, resOptions);

      if (!filePath) {
        throw new Error('Не передан путь');
      }
      new Promise((resolve, reject) => {
        fs.stat(filePath, (err, stat) => {
          if (err) reject(err);
          resolve(stat)
        })

      })
        .then(stat => {
          if (!stat.isFile()) return Promise.reject('is directory');

          if (status == 206) {
            resOptions.start = typeof resOptions.offset !== 'undefined' ? parseInt(resOptions.offset, 10) : 0;
            if (isNaN(resOptions.start)) resOptions.start = 0;
            if (typeof resOptions.length !== 'undefined') {
              resOptions.length = parseInt(resOptions.length, 10);
              if (!isNaN(resOptions.length)) {
                resOptions.end = resOptions.length + resOptions.start;
              }
            }
          }

          // Пишем заголовки
          // const status = resHeaders[':status'] || 200;
          // delete resHeaders[':status'];
          response.writeHead(status, resHeaders);

          const readStream = fs.createReadStream(filePath, resOptions);
          readStream.pipe(response);
          readStream.on('error', (err) => {
            reject(err);
          });

        })
        .catch((err) => {
          console.log(err);

          // ToDo: Хз, тут потом сделаю что-нибудь нормальное
          if (err.code === 'ENOENT') {
            response.writeHead(404, {server: serverName});
          } else {
            response.writeHead(500, {server: serverName});
          }
          response.end();
        });
      return true;
    }

    return false;
  }

  /**
   * Запуск сервера HTTP/2
   * @private
   */
  //  Todo: временно выпилено!!!!
  // __runServerHttp2 (config) {
  //   let options = {
  //     SNICallback: (serverName, cb) => {
  //       let vhost = this.__getVirtualHost(serverName, config.port);
  //       if(vhost !== false){
  //         if(typeof vhost.secureContext === 'undefined'){
  //           throw new Error(`${serverName}: не задан secureContext`);
  //           return;
  //         }
  //         return cb(null, vhost.secureContext);
  //       }
  //     },
  //     allowHTTP1: true
  //   };
  //
  //   if(typeof config.key !== 'undefined'){
  //     options.key = fs.readFileSync(config.key);
  //   }
  //   if(typeof config.cert !== 'undefined'){
  //     options.cert = fs.readFileSync(config.cert);
  //   }
  //
  //   const server = http2.createSecureServer(options);
  //   server.on('error', (err) => console.error(err));
  //   server.on('stream', async (stream, headers) => {
  //     if(this.mwStream.length) {
  //       await this.__runMiddleware(this.mwStream, 0, stream, headers);
  //     }
  //     //this.__respond(stream, headers, server, config);
  //   });
  //   server.on('request', async (request, response) => {
  //     if(this.mwRequest.length) {
  //       await this.__runMiddleware(this.mwRequest, 0, request, response);
  //     }
  //     this.__respond(request, response, config);
  //   });
  //
  //   server.listen(config.port, () => {
  //     console.log(`Сервер HTTP/2 слушает на порту ${config.port}`.green);
  //   });
  //
  //   this.__started(server, config);
  //
  //   return server;
  // }

  __runMiddleware(middlewareAr, offset = 0, ...args) {
    return new Promise((resolve, reject) => {
      if (typeof middlewareAr[offset] === 'function') {
        middlewareAr[offset](args[0], args[1], () => {
          this.__runMiddleware(middlewareAr, offset + 1, args[0], args[1]).then(() => {
            resolve();
          });
        })
      } else {
        resolve();
      }
    });
  }

  /**
   * Запуск сервера HTTP/1
   * @private
   */
  __runServerHttp1(config) {
    const server = http.createServer(async (request, response) => {
      //  Преобразуем запрос в формат HTTP/2
      //http1ToHttp2(request, response); // Временно выпилено!!
      //const { stream, headers } = http1ToHttp2(request, response);

      if (this.mwRequest.length) {
        await this.__runMiddleware(this.mwRequest, 0, request, response);
      }

      this.__respond(request, response, config);
    });

    server.listen(config.port, () => {
      console.log(`Сервер HTTP/1 слушает на порту ${config.port}`.green);
    });

    this.__started(server, config);

    return server;
  }
}