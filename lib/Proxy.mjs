/**
 * Node.js Server
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin | Интернет-агентство IT People
 * @link https://itpeople.ru
 */

import http from 'http';
import http2 from 'http2';

const optionsDefault = {
  hostname: '127.0.0.1',  //  Проксируем на localhost
  port: 8080,
  secure: false,           //  Используем для запроса http1
  passHeaders: true,
};

export default class Proxy {
  constructor (options={}) {
    this.options = Object.assign({}, optionsDefault, options);
    this.client = null;

    if(this.options.secure === true){
      this.__connect();
    }
  }

  request (request, reqponse) {
    if(this.options.secure === true){
      this.__requestHttp2(request, reqponse);
    }else{
      this.__requestHttp1(request, reqponse);
    }
  }

  __connect () {
    this.client = http2.connect(`https://${options.hostname}:${options.port}`);
  }

  __requestHttp2 (request, reqponse) {
    console.log('Запрос HTTP/2 пока не поддерживается');
    //  Тут будет обработка запроса HTTP/2
    // const req = this.client.request({ [HTTP2_HEADER_PATH]: '/' });
    // req.on('response', (headers) => {
    //   console.log(headers[HTTP2_HEADER_STATUS]);
    //   req.on('data', (chunk) => { /* .. */ });
    //   req.on('end', () => { /* .. */ });
    // });
  }

  __requestHttp1 (request, reqponse) {
    let hostname = request.headers[http2.constants.HTTP2_HEADER_AUTHORITY];
    if(hostname.indexOf(':') !== -1){
      hostname = hostname.split(':')[0];
    }

    let options = {
      hostname: this.options.hostname,
      port: this.options.port,
      path: request.headers[http2.constants.HTTP2_HEADER_PATH],
      method: request.headers[http2.constants.HTTP2_HEADER_METHOD],
      headers: {}
      /*headers: {
        'Host': hostname
        //'Content-Type': 'application/x-www-form-urlencoded',
        //'Content-Length': Buffer.byteLength(postData)
      }*/
    };

    /*if(this.options.passHeaders === true){
      options.headers = this.__headersHttp2ToHttp1(headers);
    }*/

    options.headers.Host = hostname;

    const req = http.request(options, (res) => {
      res.setEncoding('utf8');

      let data = '';
      res.on('data', chunk => data += chunk);
      res.on('end', () => {
        let resHeaders = Object.assign({}, res.headers);
        delete resHeaders.connection;
        delete resHeaders['transfer-encoding'];

       /// console.log(resHeaders);

        response.stream.respond(resHeaders);
        //response.stream.respond({ 'content-type': 'text/html; charser=utf-8', ':status': res.statusCode });
        response.stream.end(data);
      });
    });

    req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`);
    });
    req.end();
  }
  /*
  __headersHttp2ToHttp1 (headers) {
    let headers = {},
      headerRelations = {
        'accept-encoding': 'Accept-Encoding',
        'user-agent': 'User-Agent',
        'accept': 'Accept',
        'referer': 'Referer',
        'accept-language': 'Accept-Language',
        'cookie': 'Cookie',
        'range': 'Range'
      };
  }
  */
}