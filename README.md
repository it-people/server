# Node.js HTTP/2 Server

Сервер Node.js на основе HTTP/2. Имеется поддержка виртуальных хостов, некоторые функции реверс-прокси. 

Возможен запуск как с системе под управлением pm2, так и внутри отдельного проекта.

### Испольование:

```js
import { Server } from '@itpeople/server';

const server = new Server();

server.addServer({ port: 443, secure: true });
server.addServer({ port: 8080 });

//  В качестве виртуального хоста можно передать объект
import vhostObject from './vhost.mjs';
server.addVirtualHost(vhostObject);

//  Либо абсолютный путь
server.addVirtualHost(path.resolve('./vhost2.mjs'));

//  Запуск сервера
server.run();
```

### Пример кода виртуального хоста:

```js
import fs from 'fs';
import path from 'path';
import tls from 'tls';

const dirname = path.dirname(decodeURI(new URL(import.meta.url).pathname)).replace(/^\/([A-Z]):\//, '$1:/');

export default {
  name: 'site.ru',
  match: /(www\.)?site\.ru/i,	//	По этому выражению будет проверяться хост
  staticPath: path.resolve(dirname, './'),	//	Если параметр задан, то файлы из этой директории будут отдаваться как статика
  port: [80, 443],	//	Если параметр задан, то виртуальный хост будет отзываться только по указанным портам
  secureContext: tls.createSecureContext({	//	Если используется безопасное соединение, то нужно передать Secure Context для хоста
    key: fs.readFileSync(path.resolve('./site.ru.key')),
    cert: fs.readFileSync(path.resolve('./site.ru.crt'))
  }),
  started: (server, config) => {	//  Хук выполнится когда сервер был запущен
    
  },
  respond: (stream, headers) => {	//	Хук выполняется на "on stream" сервера, если не был отдан статический файл из директории staticPath
    stream.respond({ 'content-type': 'text/html; charser=utf-8', ':status': 200 });
    stream.end(fs.readFileSync(path.resolve(dirname, './index.html')));
  }
};
```